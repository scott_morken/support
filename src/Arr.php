<?php

namespace Smorken\Support;

class Arr extends \Illuminate\Support\Arr implements \Smorken\Support\Contracts\Arr
{
    /**
     * @return int next page
     */
    public static function chunk(array $data, callable $callback, int $count = 100, int $page = 0): int
    {
        $continue = true;
        do {
            $chunk = array_slice($data, $count * $page, $count, true);
            if ($chunk) {
                $r = $callback($chunk);
                if ($r === false) {
                    $continue = false;
                }
            } else {
                $continue = false;
            }
            $page++;
        } while ($continue === true);

        return $page;
    }

    /**
     * Flip an array from what is typical in an array of inputs
     * name[column][index] -> value to [index][column] -> value
     */
    public static function flip(array $arr): array
    {
        $flipped = [];
        foreach ($arr as $key => $index_values) {
            foreach ($index_values as $index => $value) {
                $flipped[$index][$key] = $value;
            }
        }

        return $flipped;
    }

    public static function inputOptions(iterable $items, string $key = 'id', $value = null, array $prepend = []): array
    {
        $get_value = function ($model, $item) {
            if ($item instanceof \Closure) {
                return $item($model);
            }

            return $item ? $model->$item : (string) $model;
        };
        $sl = [];
        if ($prepend) {
            $sl = $prepend;
        }
        foreach ($items as $m) {
            $k = $get_value($m, $key);
            $v = $get_value($m, $value);
            if ($k && $v) {
                $sl[$k] = $v;
            }
        }

        return $sl;
    }

    /**
     * Converts an array to a definition list
     */
    public static function pp(array $data): string
    {
        $escape = self::escapeFunction();
        $internal = function ($data) use ($escape, &$internal) {
            $str = '';
            if (is_array($data)) {
                foreach ($data as $k => $v) {
                    $str .= sprintf('<dt>%s</dt>', $escape($k));
                    if (is_array($v)) {
                        $str .= PHP_EOL.sprintf('<dd>%s%s</dd>', PHP_EOL, $internal($v)).PHP_EOL;
                    } else {
                        $str .= sprintf('<dd>%s</dd>', $escape($v)).PHP_EOL;
                    }
                }
            } elseif (is_string($data)) {
                $str .= sprintf('<dt></dt><dd>%s</dd>', $escape($data)).PHP_EOL;
            }

            return $str;
        };

        return sprintf('<dl>%s%s</dl>%s', PHP_EOL, $internal($data), PHP_EOL);
    }

    /**
     * Returns a string suitable for printing to the console or wrapping pre tags
     */
    public static function stringify(array $arr, int $depth = 0): string
    {
        $escape = self::escapeFunction();
        $stringy = function ($v) {
            if (is_null($v)) {
                return 'null';
            }
            if (is_a($v, \BackedEnum::class)) {
                return $v->value.' (Enum: '.$v::class.')';
            }
            if (is_a($v, \UnitEnum::class)) {
                return $v->name.' (Enum: '.$v::class.')';
            }
            if ($v === false) {
                return 'false';
            }
            if ($v === true) {
                return 'true';
            }
            if ($v === '') {
                return "''";
            }

            return (string) $v;
        };
        $str = '';
        $pre = str_repeat(' ', $depth * 4);
        if (! $arr) {
            if (is_array($arr)) {
                return $pre.'- empty -'.PHP_EOL;
            }

            return $pre.$str.PHP_EOL;
        }
        $template = $pre.'%s: %s'.PHP_EOL;
        $key_template = $pre.'[ %s ](%d)'.PHP_EOL;
        foreach ($arr as $k => $v) {
            if (is_array($v)) {
                $str .= sprintf($key_template, $escape($k), count($v));
                $str .= self::stringify($v, $depth + 1);
            } else {
                $str .= sprintf($template, $escape($k), $escape($stringy($v)));
            }
        }

        return $str;
    }

    protected static function escapeFunction(): callable
    {
        return function_exists('e') ? 'e' : function ($value) {
            if (is_a($value, \BackedEnum::class)) {
                $value = $value->value.' (Enum: '.$value::class.')';
            }
            if (is_a($value, \UnitEnum::class)) {
                return $value->name.' (Enum: '.$value::class.')';
            }

            return htmlspecialchars($value, ENT_QUOTES, 'UTF-8', true);
        };
    }
}
