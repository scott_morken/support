<?php

namespace Smorken\Support;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Foundation\CachesRoutes;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

abstract class LoadRoutes implements \Smorken\Support\Contracts\LoadRoutes
{
    protected Application|Container|null $app = null;

    protected bool $loaded = false;

    protected array $parts = [
        'middleware' => null,
        'prefix' => null,
        'controller' => null,
        'domain' => null,
        'name' => null,
    ];

    abstract protected function loadRoutes(Router $router): void;

    public function __call(string $name, array $args): Contracts\LoadRoutes
    {
        return $this->setRouteAttribute($name, $args[0] ?? null);
    }

    public function load(
        Application|Container|null $app = null,
        bool $forceLoad = false
    ): void {
        $app = $this->ensureApp($app);
        if (! ($app instanceof CachesRoutes && $app->routesAreCached()) || $forceLoad) {
            $routeAttributes = $this->getRouteAttributes();
            Route::group($routeAttributes, function (Router $router) {
                $this->loadRoutes($router);
            });
        }
        $this->loaded = true;
    }

    public function setRouteAttribute(string $key, mixed $value): Contracts\LoadRoutes
    {
        $this->verifyNotLoaded();
        if (array_key_exists($key, $this->parts)) {
            $this->parts[$key] = $value;
        }

        return $this;
    }

    public function setRouteAttributes(array $attributes): Contracts\LoadRoutes
    {
        foreach ($attributes as $k => $v) {
            $this->setRouteAttribute($k, $v);
        }

        return $this;
    }

    protected function ensureApp(Application|Container|null $app): Application|Container
    {
        if ($app) {
            $this->setApp($app);
        }

        return $this->getApp();
    }

    protected function getApp(): Container|Application
    {
        if (! $this->app) {
            throw new \OutOfBoundsException('App is not set.');
        }

        return $this->app;
    }

    public function setApp(Container|Application $app): Contracts\LoadRoutes
    {
        $this->app = $app;

        return $this;
    }

    protected function getRouteAttributes(): array
    {
        return array_filter($this->parts);
    }

    protected function verifyNotLoaded(): void
    {
        if ($this->loaded) {
            throw new \OutOfBoundsException('Routes are already loaded. Use the setters before loading.');
        }
    }
}
