<?php

namespace Smorken\Support;

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

class Routes implements \Smorken\Support\Contracts\Routes
{
    public static ?Router $router = null;

    /**
     * Override to set standard routes
     */
    public static array $standardCrudRoutes = [
        'get|/' => 'index',
        'get|view/{id}' => 'view',
        'get|create' => 'create',
        'post|create' => 'doCreate',
        'get|update/{id}' => 'update',
        'post|update/{id}' => 'doUpdate',
        'post|save/{id?}' => 'doSave',
        'get|delete/{id}' => 'delete',
        'delete|delete/{id}' => 'doDelete',
    ];

    public static array $standardResourceRoutes = [
        'get|/' => 'index',
        'get|/{id}' => 'show',
        'get|/create' => 'create',
        'post|/' => 'store',
        'get|/{id}/edit' => 'edit',
        'put|/{id}' => 'update',
        'get|/{id}/deleting' => 'deleting',
        'delete|/{id}' => 'destroy',
    ];

    /**
     * @param  array  $exclude  ['index', 'view', ...]
     * @param  array  $add  ['get|go' => 'go', 'post|go' => 'doGo', ...]
     */
    public static function create(string $controller, array $exclude = [], array $add = []): array
    {
        return self::createFrom($controller, self::$standardCrudRoutes, $exclude, $add);
    }

    public static function createFrom(
        string $controller,
        array $baseRoutes,
        array $exclude = [],
        array $add = []
    ): array {
        $routes = array_diff($baseRoutes, $exclude);
        $routes = array_replace($routes, $add);
        $routes = self::setActions($controller, $routes);
        foreach ($routes as $route => $action) {
            self::make($route, $action);
        }

        return $routes;
    }

    public static function make(string $route_key, string|array $action): void
    {
        [$method, $route] = self::getRouteParts($route_key);
        if (self::$router) {
            call_user_func([self::$router, $method], $route, $action);
        } else {
            call_user_func([Route::class, $method], $route, $action);
        }
    }

    public static function resources(string $controller, array $exclude = [], array $add = []): array
    {
        return self::createFrom($controller, self::$standardResourceRoutes, $exclude, $add);
    }

    protected static function getAction(string $controller, string $action_name): array
    {
        return [$controller, $action_name];
    }

    protected static function getRouteParts(string $route_key): array
    {
        $route_parts = explode('|', $route_key);
        if (count($route_parts) === 1) {
            $method = 'get';
            $route = $route_parts[0] ?? '';
        } else {
            [$method, $route] = $route_parts;
        }

        return [$method, $route];
    }

    protected static function setActions(string $controller, array $routes): array
    {
        $f = function (&$action_part, $route_key, $controller) {
            $action_part = self::getAction($controller, $action_part);
        };
        array_walk($routes, $f, $controller);

        return $routes;
    }
}
