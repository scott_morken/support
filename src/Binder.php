<?php

namespace Smorken\Support;

use Illuminate\Contracts\Container\Container;

class Binder implements \Smorken\Support\Contracts\Binder
{
    public function __construct(protected Container $app) {}

    public function bindAll(iterable $items): void
    {
        $contracts = $items['contract'] ?? [];
        foreach ($contracts as $contract => $concrete) {
            $this->app->bind($contract, fn ($app) => $this->fromIterable($items['concrete'] ?? [], $concrete));
        }
    }

    public function fromData(string $className, array $params, bool $create = true): mixed
    {
        $deps = $this->createDependencies($params);

        return $this->makeDependency($className, $deps, $create);
    }

    public function fromIterable(iterable $iterable, string $key): mixed
    {
        foreach ($iterable as $name => $data) {
            if ($key === $name) {
                return $this->fromData($name, $data, false);
            }
        }

        return null;
    }

    protected function createDependencies(array $params): array
    {
        $deps = [];
        foreach ($params as $param) {
            if (isset($param['impl'])) {
                $deps[] = $this->makeDependency($param['impl'], $param['params'] ?? []);
            } elseif (isset($param['bound'])) {
                $deps[] = $this->app[$param['bound']];
            } else {
                $deps[] = $param;
            }
        }

        return $deps;
    }

    protected function makeDependency(string $cls, array $params = [], bool $create = true): mixed
    {
        $refCls = new \ReflectionClass($cls);
        if ($create) {
            $params = $this->createDependencies($params);
        }

        return $refCls->newInstanceArgs($params);
    }
}
