<?php

namespace Smorken\Support\Generators;

use Smorken\Support\Str;

class Id
{
    public function make(?string $id, ?string $name, string $base): string
    {
        return $this->convertToId($this->getBase($id, $name, $base));
    }

    protected function convertToId(string $str): string
    {
        return preg_replace('/[-]{2,}/', '-',
            Str::slug(str_replace(['[', ']', '_'], '-', $str)));
    }

    protected function getBase($id, $name, $base)
    {
        return $id ?: ($name ?: $base);
    }
}
