<?php

namespace Smorken\Support;

use Illuminate\Http\Request;

class Filter implements \Smorken\Support\Contracts\Filter
{
    protected array $attributes = [];

    protected array $hide = [];

    public function __construct(array $attrs = [])
    {
        $this->setAttributes($attrs);
    }

    public static function fromArray(array $filter): static
    {
        return new static($filter);
    }

    public static function fromRequest(Request $request, array $keys): static
    {
        $attributes = $request->only($keys);

        return self::fromArray($attributes);
    }

    public function __get(string $key): mixed
    {
        return $this->getAttribute($key);
    }

    public function __set(string $key, $value): void
    {
        $this->setAttribute($key, $value);
    }

    public function all(bool $includeHidden = true): array
    {
        if ($includeHidden) {
            return $this->attributes;
        }

        return array_diff_key($this->attributes, array_fill_keys($this->hide, 0));
    }

    public function except(array $exceptions): array
    {
        return array_diff_key($this->all(false), array_fill_keys($exceptions, 0));
    }

    /**
     * @param  null  $default
     */
    public function getAttribute(string $key, $default = null): mixed
    {
        return $this->attributes[$key] ?? $default;
    }

    public function has(string $key, bool $countNull = false): bool
    {
        if ($countNull) {
            return array_key_exists($key, $this->attributes);
        }

        return isset($this->attributes[$key]);
    }

    public function hide(array|string $attributes): static
    {
        if (! is_array($attributes)) {
            $attributes = (array) $attributes;
        }
        $this->hide = $attributes;

        return $this;
    }

    public function isEmpty(string $key): bool
    {
        $value = $this->getAttribute($key);
        if ($value === null) {
            return true;
        }
        if ($value === '') {
            return true;
        }
        if (is_array($value) && empty($value)) {
            return true;
        }

        return false;
    }

    public function isNotEmpty(string $key): bool
    {
        return ! $this->isEmpty($key);
    }

    public function map(array $mapped, bool $includeAll = true): static
    {
        $data = [];
        foreach ($this->attributes as $key => $value) {
            $newKey = $mapped[$key] ?? null;
            if ($newKey) {
                $data[$newKey] = $value;
            }
            if ($newKey === null && $includeAll) {
                $data[$key] = $value;
            }
        }
        $newFilter = new static($data);
        if ($this->hide) {
            $hidden = [];
            foreach ($this->hide as $key) {
                $hidden[] = $mapped[$key] ?? $key;
            }
            $newFilter->hide($hidden);
        }

        return $newFilter;
    }

    public function only(array $keys): array
    {
        $only = [];
        foreach ($keys as $key) {
            $only[$key] = $this->getAttribute($key);
        }

        return $only;
    }

    /**
     * Create query string from the requested keys
     */
    public function onlyToQueryString(array $keys): string
    {
        return http_build_query($this->only($keys));
    }

    public function setAttribute(string $key, $value): void
    {
        $this->attributes[$key] = $value;
    }

    public function setAttributes(iterable $attributes): void
    {
        foreach ($attributes as $k => $v) {
            $this->setAttribute($k, $v);
        }
    }

    public function toArray(): array
    {
        return (array) $this->all(false);
    }

    /**
     * Create a query string from all keys
     */
    public function toQueryString(): string
    {
        return $this->onlyToQueryString(array_keys($this->attributes));
    }
}
