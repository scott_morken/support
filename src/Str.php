<?php

namespace Smorken\Support;

class Str extends \Illuminate\Support\Str implements \Smorken\Support\Contracts\Str
{
    /**
     * {@inheritDoc}
     */
    public static function friendlyName(string $value, array $replace = ['_', '-']): string
    {
        $swap = function ($v) {
            $replacements = ['id' => 'ID'];
            $v = $replacements[strtolower($v)] ?? $v;

            return $v;
        };

        return implode(' ',
            array_map($swap, explode(' ', self::title(str_replace($replace, ' ', self::snake($value))))));
    }
}
