<?php

declare(strict_types=1);

namespace Smorken\Support\Constants\Attributes;

#[\Attribute(\Attribute::TARGET_CLASS_CONSTANT)]
class EnumLabel
{
    public function __construct(readonly public string $label) {}
}
