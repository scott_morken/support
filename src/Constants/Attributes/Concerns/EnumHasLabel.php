<?php

declare(strict_types=1);

namespace Smorken\Support\Constants\Attributes\Concerns;

use Illuminate\Support\Str;
use Smorken\Support\Constants\Attributes\EnumLabel;

trait EnumHasLabel
{
    public static function toArray(): array
    {
        $arr = [];
        foreach (self::cases() as $case) {
            $key = is_a($case, \BackedEnum::class) ? $case->value : $case->name;
            $arr[$key] = $case->label();
        }

        return $arr;
    }

    public function label(): string
    {
        $r = new \ReflectionEnum(static::class);
        $attributes = $r->getCase($this->name)->getAttributes();
        $filtered = array_filter($attributes, fn (\ReflectionAttribute $a) => $a->getName() === EnumLabel::class);
        $first = $filtered[0] ?? null;
        if ($first) {
            return $first->newInstance()->label;
        }
        if (is_a($this, \BackedEnum::class)) {
            return Str::studly($this->value);
        }

        return Str::studly(strtolower($this->name));
    }
}
