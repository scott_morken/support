<?php

namespace Smorken\Support\Contracts;

interface Binder
{
    public function bindAll(iterable $items): void;

    public function fromIterable(iterable $iterable, string $key): mixed;
}
