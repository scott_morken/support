<?php

namespace Smorken\Support\Contracts;

use Illuminate\Contracts\Container\Container;
use Illuminate\Contracts\Foundation\Application;

/**
 * @method $this middleware(array|string $middleware)
 * @method $this prefix(string $prefix)
 * @method $this controller(string $controllerClass)
 * @method $this domain(string $domain)
 * @method $this name(string $prefixName)
 */
interface LoadRoutes
{
    public function load(
        Application|Container|null $app = null,
        bool $forceLoad = false
    ): void;

    public function setApp(Application|Container $app): self;

    public function setRouteAttribute(string $key, mixed $value): self;

    public function setRouteAttributes(array $attributes): self;
}
