<?php

namespace Smorken\Support\Contracts;

use Illuminate\Http\Request;

/**
 * @phpstan-require-extends \Smorken\Support\Filter
 */
interface Filter
{
    public function __construct(array $attrs = []);

    public function all(bool $includeHidden = true): array;

    public function except(array $exceptions): array;

    public function getAttribute(string $key, $default = null): mixed;

    public function has(string $key, bool $countNull = false): bool;

    public function hide(array|string $attributes): static;

    public function isEmpty(string $key): bool;

    public function isNotEmpty(string $key): bool;

    public function map(array $mapped, bool $includeAll = true): static;

    public function only(array $keys): array;

    /**
     * Create query string from the requested keys
     */
    public function onlyToQueryString(array $keys): string;

    public function setAttribute(string $key, $value): void;

    public function setAttributes(iterable $attributes): void;

    public function toArray(): array;

    /**
     * Create a query string from all keys
     */
    public function toQueryString(): string;

    public static function fromArray(array $filter): static;

    public static function fromRequest(Request $request, array $keys): static;
}
