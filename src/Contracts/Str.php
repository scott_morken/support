<?php

namespace Smorken\Support\Contracts;

interface Str
{
    public static function friendlyName(string $value, array $replace = ['_', '-']): string;
}
