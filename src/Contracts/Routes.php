<?php

namespace Smorken\Support\Contracts;

/**
 * Interface Routes
 *
 *
 * @static array $standard_routes
 */
interface Routes
{
    /**
     * @param  array  $exclude  ['index', 'view', ...]
     * @param  array  $add  ['get|go' => 'go', 'post|go' => 'doGo', ...]
     */
    public static function create(string $controller, array $exclude = [], array $add = []): array;

    public static function make(string $route_key, string|array $action): void;
}
