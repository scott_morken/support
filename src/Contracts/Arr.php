<?php

namespace Smorken\Support\Contracts;

interface Arr
{
    public static function inputOptions(iterable $items, string $key = 'id', $value = null, array $prepend = []): array;

    /**
     * Pass chunks of an array to a callback
     *
     * @return int next page
     */
    public static function chunk(array $data, callable $callback, int $count = 100, int $page = 0): int;

    /**
     * Flip an array from what is typical in an array of inputs
     * name[column][index] -> value to [index][column] -> value
     */
    public static function flip(array $arr): array;

    public static function pp(array $data): string;

    public static function stringify(array $arr, int $depth = 0): string;
}
