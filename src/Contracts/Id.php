<?php

namespace Smorken\Support\Contracts;

interface Id
{
    public static function make(?string $id = null, ?string $name = null, string $base = 'input'): ?string;

    /**
     * Resets the id trackers
     */
    public static function reset(): void;
}
