<?php

namespace Smorken\Support;

class Id implements \Smorken\Support\Contracts\Id
{
    protected static array $keys = [];

    protected static array $used = [];

    public static function make(?string $id = null, ?string $name = null, string $base = 'input'): ?string
    {
        if ($id === null) {
            return null;
        }
        $base_id = (new \Smorken\Support\Generators\Id)->make($id, $name, $base);
        if (self::needsKey($id, $name) || self::isUsed($base_id)) {
            do {
                $key = self::getNextKey($base_id);
                $id = sprintf('%s-%s', $base_id, $key);
            } while (self::isUsed($id));
            $base_id = $id;
        }
        self::$used[$base_id] = true;

        return $base_id;
    }

    public static function reset(): void
    {
        self::$keys = [];
        self::$used = [];
    }

    protected static function getNextKey(string $base_id): int
    {
        if (! isset(self::$keys[$base_id])) {
            self::$keys[$base_id] = 0;
        }

        return ++self::$keys[$base_id];
    }

    protected static function isUsed(string $id): bool
    {
        return isset(self::$used[$id]);
    }

    protected static function needsKey(?string $id, ?string $name): bool
    {
        if (! $id && ! $name) {
            return true;
        }
        if ($id) {
            return isset(self::$used[$id]);
        }
        if ($name) {
            if (Str::endsWith($name, '[]')) {
                return true;
            }
        }

        return false;
    }
}
