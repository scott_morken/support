<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/12/15
 * Time: 8:40 AM
 */

namespace Smorken\Support;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->registerHealthRoutes();
    }

    public function register(): void
    {
        $this->bindBinder();
    }

    protected function bindBinder(): void
    {
        $this->app->bind(\Smorken\Support\Contracts\Binder::class, fn ($app) => new \Smorken\Support\Binder($app));
    }

    protected function registerHealthRoutes(): void
    {
        $this->loadRoutesFrom(__DIR__.'/../routes/health.php');
    }
}
