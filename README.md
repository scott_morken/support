## Support package for Laravel 6+

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation

The service provider should automatically register.
If not, you can manually add `Smorken\Support\ServiceProvider::class` to the
providers section of `config/app.php`.

#### Binder

The binder is a method to automatically wire dependencies from 
a config array. I use it primarily for wiring storage to models.

The config array contains a `concrete` key and a `contract` key.
The `concrete` key can contain all of the possible concrete implementations
that your project contains for a contract.  The `contract` key
contains the actual implementation to be used.

##### Simple dependency example
```php
$config = [
    'concrete' => [
        ImplOne::class => [
            'foo' => 'foo impl one', //parameters for ImplOne
        ],
    ],
    'contract' => [
        ContractOne::class => ImplOne::class,
    ],
];
$binder->bindAll($config);
```

##### Create dependencies by reflection
```php
$config = [
    'concrete' => [
        ImplOne::class => [
            'foo' => 'foo impl one',
        ],
        ImplTwo::class => [
            'foo' => 'foo impl two', //param one
            'bar' => [ //param two (reflection)
                'impl'   => ImplBar::class,
                'params' => [
                    'bar' => 'bar from impl bar in impl two',
                ],
            ],
        ],
    ],
    'contract' => [
        ContractOne::class => ImplTwo::class,
    ],
];
$binder->bindAll($config);
```

##### Create dependencies from container
```php
$config = [
    'concrete' => [
        ImplBar::class => [
            'bar' => 'bar from prebound impl bar in impl two',
        ],
        ImplTwo::class => [
            'foo' => 'foo impl two prebound', //param one
            'bar' => [ //param two (container)
                'bound' => ContractBar::class,
            ],
        ],
    ],
    'contract' => [
        ContractBar::class => ImplBar::class,
        ContractOne::class => ImplTwo::class,
    ],
];
$binder->bindAll($config);
```
