<?php

namespace Tests\Smorken\Support\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\Support\Id;

class IdTest extends TestCase
{
    public function testIdReplaceUnderscores()
    {
        $id = Id::make('foo_bar', null);
        $this->assertEquals('foo-bar', $id);
    }

    public function testNameReplaceUnderscores()
    {
        Id::reset();
        $id = Id::make(null, 'foo_bar');
        $this->assertEquals('foo-bar', $id);
    }

    public function testBaseDefault()
    {
        Id::reset();
        $id = Id::make(null, null);
        $this->assertEquals('input-1', $id);
    }

    public function testBaseDefaultIncrements()
    {
        Id::reset();
        $id = Id::make(null, null);
        $this->assertEquals('input-1', $id);
        $this->assertEquals('input-2', Id::make(null, null));
    }

    public function testNameWithArrayIncrements()
    {
        Id::reset();
        $name = 'foo[bar][]';
        $this->assertEquals('foo-bar-1', Id::make(null, $name));
        $this->assertEquals('foo-bar-2', Id::make(null, $name));
        $this->assertEquals('foo-bar-3', Id::make(null, $name));
    }

    public function testIdAndNameWithArrayIncrementsWithUsedId()
    {
        Id::reset();
        $name = 'foo[bar][]';
        $this->assertEquals('foo-bar-1', Id::make('foo-bar-1', $name));
        $this->assertEquals('foo-bar-2', Id::make(null, $name));
        $this->assertEquals('foo-bar-3', Id::make(null, $name));
    }
}
