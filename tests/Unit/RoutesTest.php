<?php

namespace Tests\Smorken\Support\Unit;

use Illuminate\Routing\Router;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Support\Routes;

class RoutesTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        Routes::$router = m::mock(Router::class);
    }

    public function testRouteCreationWithAdd(): void
    {
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('/', ['Controller', 'index']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('view/{id}', ['Controller', 'view']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('create', ['Controller', 'create']);
        Routes::$router->shouldReceive('post')
            ->once()
            ->with('create', ['Controller', 'doCreate']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('update/{id}', ['Controller', 'update']);
        Routes::$router->shouldReceive('post')
            ->once()
            ->with('update/{id}', ['Controller', 'doUpdate']);
        Routes::$router->shouldReceive('post')
            ->once()
            ->with('save/{id?}', ['Controller', 'doSave']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('delete/{id}', ['Controller', 'delete']);
        Routes::$router->shouldReceive('delete')
            ->once()
            ->with('delete/{id}', ['Controller', 'doDelete']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('go', ['Controller', 'goGo']);
        Routes::$router->shouldReceive('post')
            ->once()
            ->with('go', ['Controller', 'doGoGo']);

        $routes = Routes::create('Controller', [], ['get|go' => 'goGo', 'post|go' => 'doGoGo']);
        $expected = [
            'get|/' => ['Controller', 'index'],
            'get|view/{id}' => ['Controller', 'view'],
            'get|create' => ['Controller', 'create'],
            'post|create' => ['Controller', 'doCreate'],
            'get|update/{id}' => ['Controller', 'update'],
            'post|update/{id}' => ['Controller', 'doUpdate'],
            'post|save/{id?}' => ['Controller', 'doSave'],
            'get|delete/{id}' => ['Controller', 'delete'],
            'delete|delete/{id}' => ['Controller', 'doDelete'],
            'get|go' => ['Controller', 'goGo'],
            'post|go' => ['Controller', 'doGoGo'],
        ];
        $this->assertEquals($expected, $routes);
    }

    public function testRouteCreationWithExclude(): void
    {
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('/', ['Controller', 'index']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('create', ['Controller', 'create']);
        Routes::$router->shouldReceive('post')
            ->once()
            ->with('create', ['Controller', 'doCreate']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('update/{id}', ['Controller', 'update']);
        Routes::$router->shouldReceive('post')
            ->once()
            ->with('update/{id}', ['Controller', 'doUpdate']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('delete/{id}', ['Controller', 'delete']);
        Routes::$router->shouldReceive('delete')
            ->once()
            ->with('delete/{id}', ['Controller', 'doDelete']);
        $routes = Routes::create('Controller', ['view', 'doSave']);
        $expected = [
            'get|/' => ['Controller', 'index'],
            'get|create' => ['Controller', 'create'],
            'post|create' => ['Controller', 'doCreate'],
            'get|update/{id}' => ['Controller', 'update'],
            'post|update/{id}' => ['Controller', 'doUpdate'],
            'get|delete/{id}' => ['Controller', 'delete'],
            'delete|delete/{id}' => ['Controller', 'doDelete'],
        ];
        $this->assertEquals($expected, $routes);
    }

    public function testStandardRouteCreation(): void
    {
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('/', ['FooController', 'index']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('view/{id}', ['FooController', 'view']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('create', ['FooController', 'create']);
        Routes::$router->shouldReceive('post')
            ->once()
            ->with('create', ['FooController', 'doCreate']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('update/{id}', ['FooController', 'update']);
        Routes::$router->shouldReceive('post')
            ->once()
            ->with('update/{id}', ['FooController', 'doUpdate']);
        Routes::$router->shouldReceive('post')
            ->once()
            ->with('save/{id?}', ['FooController', 'doSave']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('delete/{id}', ['FooController', 'delete']);
        Routes::$router->shouldReceive('delete')
            ->once()
            ->with('delete/{id}', ['FooController', 'doDelete']);
        $routes = Routes::create('FooController');
        $expected = [
            'get|/' => ['FooController', 'index'],
            'get|view/{id}' => ['FooController', 'view'],
            'get|create' => ['FooController', 'create'],
            'post|create' => ['FooController', 'doCreate'],
            'get|update/{id}' => ['FooController', 'update'],
            'post|update/{id}' => ['FooController', 'doUpdate'],
            'post|save/{id?}' => ['FooController', 'doSave'],
            'get|delete/{id}' => ['FooController', 'delete'],
            'delete|delete/{id}' => ['FooController', 'doDelete'],
        ];
        $this->assertEquals($expected, $routes);
    }

    public function testResourcesRouteCreation(): void
    {
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('/', ['FooController', 'index']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('/{id}', ['FooController', 'show']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('/create', ['FooController', 'create']);
        Routes::$router->shouldReceive('post')
            ->once()
            ->with('/', ['FooController', 'store']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('/{id}/edit', ['FooController', 'edit']);
        Routes::$router->shouldReceive('put')
            ->once()
            ->with('/{id}', ['FooController', 'update']);
        Routes::$router->shouldReceive('get')
            ->once()
            ->with('/{id}/deleting', ['FooController', 'deleting']);
        Routes::$router->shouldReceive('delete')
            ->once()
            ->with('/{id}', ['FooController', 'destroy']);
        $routes = Routes::resources('FooController');
        $expected = [
            'get|/' => ['FooController', 'index'],
            'get|/{id}' => ['FooController', 'show'],
            'get|/create' => ['FooController', 'create'],
            'post|/' => ['FooController', 'store'],
            'get|/{id}/edit' => ['FooController', 'edit'],
            'put|/{id}' => ['FooController', 'update'],
            'get|/{id}/deleting' => ['FooController', 'deleting'],
            'delete|/{id}' => ['FooController', 'destroy'],
        ];
        $this->assertEquals($expected, $routes);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
