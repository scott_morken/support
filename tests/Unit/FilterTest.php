<?php

namespace Tests\Smorken\Support\Unit;

use Illuminate\Http\Request;
use PHPUnit\Framework\TestCase;
use Smorken\Support\Filter;

class FilterTest extends TestCase
{
    public function testExceptRemoves(): void
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => 3]);
        $this->assertEquals(['bar' => 2], $sut->except(['foo', 'biz']));
    }

    public function testFromRequest(): void
    {
        $request = (new Request)->merge(['foo' => 'hello', 'bar' => 1, 'biz' => null]);
        $sut = Filter::fromRequest($request, ['foo', 'bar', 'biz']);
        $this->assertEquals([
            'foo' => 'hello',
            'bar' => 1,
            'biz' => null,
        ], $sut->toArray());
    }

    public function testFromRequestCanLimitKeys(): void
    {
        $request = (new Request)->merge(['foo' => 'hello', 'bar' => 1, 'biz' => null]);
        $sut = Filter::fromRequest($request, ['foo', 'biz']);
        $this->assertEquals([
            'foo' => 'hello',
            'biz' => null,
        ], $sut->toArray());
    }

    public function testHasWithKeyAndNullValue(): void
    {
        $sut = new Filter(['foo' => null, 'bar' => 2, 'biz' => 3]);
        $this->assertFalse($sut->has('foo'));
    }

    public function testHasWithKeyAndNullValueAndCountNull(): void
    {
        $sut = new Filter(['foo' => null, 'bar' => 2, 'biz' => 3]);
        $this->assertTrue($sut->has('foo', true));
    }

    public function testHasWithKeyAndValue(): void
    {
        $sut = new Filter(['foo' => 0, 'bar' => 2, 'biz' => 3]);
        $this->assertTrue($sut->has('foo'));
    }

    public function testHasWithoutKey(): void
    {
        $sut = new Filter(['foo' => 0, 'bar' => 2, 'biz' => 3]);
        $this->assertFalse($sut->has('buz'));
    }

    public function testHideAsStringHidesAttributeFromAll(): void
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => 3]);
        $sut->hide('bar');
        $this->assertEquals(['foo' => 1, 'biz' => 3], $sut->all(false));
    }

    public function testHideHidesAttributeFromAll(): void
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => 3]);
        $sut->hide(['bar']);
        $this->assertEquals(['foo' => 1, 'biz' => 3], $sut->all(false));
    }

    public function testHideHidesAttributeFromExcept(): void
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => 3]);
        $sut->hide(['bar']);
        $this->assertEquals(['foo' => 1], $sut->except(['biz']));
    }

    public function testIsEmptyIsFalseForFalse(): void
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => false]);
        $this->assertFalse($sut->isEmpty('biz'));
    }

    public function testIsEmptyIsFalseForZero(): void
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => 0]);
        $this->assertFalse($sut->isEmpty('biz'));
    }

    public function testIsEmptyIsTrueForEmptyArray(): void
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => []]);
        $this->assertTrue($sut->isEmpty('biz'));
    }

    public function testIsEmptyIsTrueForEmptyString(): void
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => '']);
        $this->assertTrue($sut->isEmpty('biz'));
    }

    public function testIsEmptyIsTrueForNull(): void
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => null]);
        $this->assertTrue($sut->isEmpty('biz'));
    }

    public function testMagicSetAndGet()
    {
        $sut = new Filter;
        $sut->foo = 1;
        $sut->bar = 2;
        $this->assertEquals(['foo' => 1, 'bar' => 2], $sut->all());
    }

    public function testMap(): void
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => 'world']);
        $this->assertEquals(['newFoo' => 1, 'bar' => 2, 'biz' => 'world'], $sut->map(['foo' => 'newFoo'])->toArray());
    }

    public function testMapWithHidden(): void
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => 'world']);
        $sut->hide(['foo']);
        $newFilter = $sut->map(['foo' => 'newFoo']);
        $this->assertEquals(['bar' => 2, 'biz' => 'world'], $newFilter->toArray());
        $this->assertEquals(['newFoo' => 1, 'bar' => 2, 'biz' => 'world'], $newFilter->all());
    }

    public function testOnlyIsOnly()
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => 3]);
        $this->assertEquals(['foo' => 1, 'biz' => 3], $sut->only(['foo', 'biz']));
    }

    public function testOnlyToQueryStringIsOnly()
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => 3]);
        $this->assertEquals('foo=1&biz=3', $sut->onlyToQueryString(['foo', 'biz']));
    }

    public function testToQueryStringKeysIsAllKeys()
    {
        $sut = new Filter(['foo' => 1, 'bar' => 2, 'biz' => 3]);
        $this->assertEquals('foo=1&bar=2&biz=3', $sut->toQueryString());
    }
}
