<?php

declare(strict_types=1);

namespace Tests\Smorken\Support\Unit\Constants;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Tests\Smorken\Support\Stubs\TestEnum;

class EnumWithLabelTest extends TestCase
{
    #[Test]
    public function it_creates_an_array(): void
    {
        $this->assertEquals([
            'ONE' => 'This is one',
            'TWO' => 'Two',
        ], TestEnum::toArray());
    }

    #[Test]
    public function it_uses_the_attribute_for_the_label(): void
    {
        $this->assertEquals('This is one', TestEnum::ONE->label());
    }

    #[Test]
    public function it_uses_the_value_for_the_label(): void
    {
        $this->assertEquals('Two', TestEnum::TWO->label());
    }
}
