<?php

declare(strict_types=1);

namespace Tests\Smorken\Support\Unit\Constants;

use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;
use Tests\Smorken\Support\Stubs\TestBackedEnum;

class BackedEnumWithLabelTest extends TestCase
{
    #[Test]
    public function it_creates_an_array(): void
    {
        $this->assertEquals([
            'one' => 'This is one',
            'two' => 'Two',
        ], TestBackedEnum::toArray());
    }

    #[Test]
    public function it_uses_the_attribute_for_the_label(): void
    {
        $this->assertEquals('This is one', TestBackedEnum::ONE->label());
    }

    #[Test]
    public function it_uses_the_value_for_the_label(): void
    {
        $this->assertEquals('Two', TestBackedEnum::TWO->label());
    }
}
