<?php

namespace Tests\Smorken\Support\Unit;

use Illuminate\Support\Collection;
use PHPUnit\Framework\TestCase;
use Smorken\Support\Arr;

class ArrTest extends TestCase
{
    public function testCanFlip()
    {
        $arr = [
            'id' => [
                0 => 'id-val1',
                1 => 'id-val2',
            ],
            'col1' => [
                0 => 'col1-val1',
                1 => 'col1-val2',
            ],
            'col2' => [
                0 => 'col2-val1',
                1 => 'col2-val2',
            ],
        ];
        $r = Arr::flip($arr);
        $expected = [
            0 => ['id' => 'id-val1', 'col1' => 'col1-val1', 'col2' => 'col2-val1'],
            1 => ['id' => 'id-val2', 'col1' => 'col1-val2', 'col2' => 'col2-val2'],
        ];
        $this->assertEquals($expected, $r);
    }

    public function testChunkWithLessThan()
    {
        $counter = 0;
        $counts = [35];
        $f = function ($items) use (&$counter, $counts) {
            $this->assertCount($counts[$counter], $items);
            $counter++;
        };
        $data = array_fill(0, 35, 'test');
        Arr::chunk($data, $f);
        $this->assertEquals(1, $counter);
    }

    public function testChunkWithMoreThan()
    {
        $counter = 0;
        $counts = [25, 10];
        $f = function ($items) use (&$counter, $counts) {
            $this->assertCount($counts[$counter], $items);
            $counter++;
        };
        $data = array_fill(0, 35, 'test');
        Arr::chunk($data, $f, 25);
        $this->assertEquals(2, $counter);
    }

    public function testChunkWithRestart()
    {
        $counter = 0;
        $counts = [25, 10];
        $f = function ($items) use (&$counter, $counts) {
            $this->assertCount($counts[$counter], $items);
            $counter++;

            return false;
        };
        $f2 = function ($items) use (&$counter, $counts) {
            $this->assertCount($counts[$counter], $items);
            $counter++;
        };
        $data = array_fill(0, 35, 'test');
        $page = Arr::chunk($data, $f, 25);
        Arr::chunk($data, $f2, 25, $page);
        $this->assertEquals(2, $counter);
    }

    public function testInputOptionSimpleArrayKeyedObject()
    {
        $options = $this->makeOptions();
        $io = Arr::inputOptions($options, 'id', 'descr');
        $expected = [1 => 'Descr 1', 2 => 'Descr 2', 3 => 'Descr 3'];
        $this->assertEquals($expected, $io);
    }

    public function testInputOptionWithCollection()
    {
        $options = $this->makeOptions();
        $collection = new Collection($options);
        $io = Arr::inputOptions($collection, 'id', 'descr');
        $expected = [1 => 'Descr 1', 2 => 'Descr 2', 3 => 'Descr 3'];
        $this->assertEquals($expected, $io);
    }

    public function testPp()
    {
        $arr = [
            'foo' => 'bar',
            'biz',
            'buz' => [
                'foo' => [
                    'bar',
                ],
                'biz' => null,
                'buz' => [
                    'foo' => [
                        'bar' => 'biz',
                    ],
                ],
                'is_empty' => [],
            ],
        ];
        $expected = <<<'DOC'
<dl>
<dt>foo</dt><dd>bar</dd>
<dt>0</dt><dd>biz</dd>
<dt>buz</dt>
<dd>
<dt>foo</dt>
<dd>
<dt>0</dt><dd>bar</dd>
</dd>
<dt>biz</dt><dd></dd>
<dt>buz</dt>
<dd>
<dt>foo</dt>
<dd>
<dt>bar</dt><dd>biz</dd>
</dd>
</dd>
<dt>is_empty</dt>
<dd>
</dd>
</dd>
</dl>

DOC;

        $result = Arr::pp($arr);
        $this->assertEquals($expected, $result);
    }

    public function testStringify()
    {
        $arr = [
            'foo' => 'bar',
            'biz',
            'buz' => [
                'foo' => [
                    'bar',
                ],
                'biz' => null,
                'buz' => [
                    'foo' => [
                        'bar' => 'biz',
                    ],
                ],
                'is_empty' => [],
            ],
        ];
        $expected = <<<'DOC'
foo: bar
0: biz
[ buz ](4)
    [ foo ](1)
        0: bar
    biz: null
    [ buz ](1)
        [ foo ](1)
            bar: biz
    [ is_empty ](0)
        - empty -

DOC;

        $result = Arr::stringify($arr);
        $this->assertEquals($expected, $result);
    }

    protected function makeOptions($count = 3)
    {
        $options = [];
        for ($i = 1; $i <= $count; $i++) {
            $o = new \stdClass;
            $o->id = $i;
            $o->descr = 'Descr '.$i;
            $options[] = $o;
        }

        return $options;
    }
}
