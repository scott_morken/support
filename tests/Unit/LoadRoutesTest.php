<?php

namespace Tests\Smorken\Support\Unit;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Tests\Smorken\Support\Stubs\LoadRoutesStub;

class LoadRoutesTest extends TestCase
{
    protected Application|\Illuminate\Contracts\Container\Container|null $app = null;

    public function testCachedCanForceLoad(): void
    {
        $sut = new LoadRoutesStub;
        copy($this->getApp()->bootstrapPath('cache/routes-v7.php.stub'),
            $this->getApp()->bootstrapPath('cache/routes-v7.php'));
        $sut->load($this->getApp(), true);
        $this->assertEquals(3, $this->getRouter()->getRoutes()->count());
        $expected = [
            [
                'uri' => '/',
                'methods' => ['GET', 'HEAD'],
                'action' => [
                    'uses' => 'FooController@index',
                    'controller' => 'FooController@index',
                    'namespace' => null,
                    'prefix' => '',
                    'where' => [],
                ],
            ],
            [
                'uri' => '/',
                'methods' => ['POST'],
                'action' => [
                    'uses' => 'FooController@doIndex',
                    'controller' => 'FooController@doIndex',
                    'namespace' => null,
                    'prefix' => '',
                    'where' => [],
                ],
            ],
            [
                'uri' => 'bar',
                'methods' => ['GET', 'HEAD'],
                'action' => [
                    'uses' => 'FooController@bar',
                    'controller' => 'FooController@bar',
                    'namespace' => null,
                    'prefix' => '',
                    'where' => [],
                ],
            ],
        ];
        foreach ($this->getRouter()->getRoutes()->getRoutes() as $i => $route) {
            $compare = $expected[$i];
            foreach ($compare as $k => $v) {
                $this->assertEquals($v, $route->$k);
            }
        }
    }

    public function testCachedDoesNotLoadRoutesByDefault(): void
    {
        $sut = new LoadRoutesStub;
        copy($this->getApp()->bootstrapPath('cache/routes-v7.php.stub'),
            $this->getApp()->bootstrapPath('cache/routes-v7.php'));
        $sut->load($this->getApp());
        $this->assertEquals([], $this->getRouter()->getRoutes()->getRoutes());
    }

    public function testNoAppSetIsException(): void
    {
        $sut = new LoadRoutesStub;
        $this->expectException(\OutOfBoundsException::class);
        $sut->load();
    }

    public function testSetterAfterLoadIsException(): void
    {
        $sut = new LoadRoutesStub;
        $sut->load($this->getApp());
        $this->expectException(\OutOfBoundsException::class);
        $sut->prefix('foo');
    }

    public function testSimpleRoute(): void
    {
        $sut = new LoadRoutesStub;
        $sut->load($this->getApp());
        $expected = [
            [
                'uri' => '/',
                'methods' => ['GET', 'HEAD'],
                'action' => [
                    'uses' => 'FooController@index',
                    'controller' => 'FooController@index',
                    'namespace' => null,
                    'prefix' => '',
                    'where' => [],
                ],
            ],
            [
                'uri' => '/',
                'methods' => ['POST'],
                'action' => [
                    'uses' => 'FooController@doIndex',
                    'controller' => 'FooController@doIndex',
                    'namespace' => null,
                    'prefix' => '',
                    'where' => [],
                ],
            ],
            [
                'uri' => 'bar',
                'methods' => ['GET', 'HEAD'],
                'action' => [
                    'uses' => 'FooController@bar',
                    'controller' => 'FooController@bar',
                    'namespace' => null,
                    'prefix' => '',
                    'where' => [],
                ],
            ],
        ];
        $this->assertEquals(3, $this->getRouter()->getRoutes()->count());
        foreach ($this->getRouter()->getRoutes()->getRoutes() as $i => $route) {
            $compare = $expected[$i];
            foreach ($compare as $k => $v) {
                $this->assertEquals($v, $route->$k);
            }
        }
    }

    public function testSimpleRouteUsingSetApp(): void
    {
        $sut = new LoadRoutesStub;
        $sut->setApp($this->getApp())->load();
        $expected = [
            [
                'uri' => '/',
                'methods' => ['GET', 'HEAD'],
                'action' => [
                    'uses' => 'FooController@index',
                    'controller' => 'FooController@index',
                    'namespace' => null,
                    'prefix' => '',
                    'where' => [],
                ],
            ],
            [
                'uri' => '/',
                'methods' => ['POST'],
                'action' => [
                    'uses' => 'FooController@doIndex',
                    'controller' => 'FooController@doIndex',
                    'namespace' => null,
                    'prefix' => '',
                    'where' => [],
                ],
            ],
            [
                'uri' => 'bar',
                'methods' => ['GET', 'HEAD'],
                'action' => [
                    'uses' => 'FooController@bar',
                    'controller' => 'FooController@bar',
                    'namespace' => null,
                    'prefix' => '',
                    'where' => [],
                ],
            ],
        ];
        $this->assertEquals(3, $this->getRouter()->getRoutes()->count());
        foreach ($this->getRouter()->getRoutes()->getRoutes() as $i => $route) {
            $compare = $expected[$i];
            foreach ($compare as $k => $v) {
                $this->assertEquals($v, $route->$k);
            }
        }
    }

    public function testWithPrefixAndMiddleware(): void
    {
        $sut = new LoadRoutesStub;
        $sut->middleware(['auth'])->prefix('foo')->load($this->getApp());
        $expected = [
            [
                'uri' => 'foo',
                'methods' => ['GET', 'HEAD'],
                'action' => [
                    'middleware' => ['auth'],
                    'uses' => 'FooController@index',
                    'controller' => 'FooController@index',
                    'namespace' => null,
                    'prefix' => 'foo',
                    'where' => [],
                ],
            ],
            [
                'uri' => 'foo',
                'methods' => ['POST'],
                'action' => [
                    'middleware' => ['auth'],
                    'uses' => 'FooController@doIndex',
                    'controller' => 'FooController@doIndex',
                    'namespace' => null,
                    'prefix' => 'foo',
                    'where' => [],
                ],
            ],
            [
                'uri' => 'foo/bar',
                'methods' => ['GET', 'HEAD'],
                'action' => [
                    'middleware' => ['auth'],
                    'uses' => 'FooController@bar',
                    'controller' => 'FooController@bar',
                    'namespace' => null,
                    'prefix' => 'foo',
                    'where' => [],
                ],
            ],
        ];
        $this->assertEquals(3, $this->getRouter()->getRoutes()->count());
        foreach ($this->getRouter()->getRoutes()->getRoutes() as $i => $route) {
            $compare = $expected[$i];
            foreach ($compare as $k => $v) {
                $this->assertEquals($v, $route->$k);
            }
        }
    }

    protected function getApp(): Application|\Illuminate\Contracts\Container\Container
    {
        if (! $this->app) {
            $this->app = new \Illuminate\Foundation\Application(realpath(__DIR__.'/../laravel'));
            $this->app['files'] = new Filesystem;
        }

        return $this->app;
    }

    protected function getRouter(): Router|m\MockInterface
    {
        if (! $this->getApp()->has('router')) {
            $router = new Router(m::mock(Dispatcher::class), $this->getApp());
            $this->getApp()['router'] = $router;
        }

        return $this->getApp()->get('router');
    }

    protected function setUp(): void
    {
        parent::setUp();
        $this->getRouter();
        Route::setFacadeApplication($this->getApp());
        if (file_exists($this->getApp()->bootstrapPath('cache/routes-v7.php'))) {
            unlink($this->getApp()->bootstrapPath('cache/routes-v7.php'));
        }
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        $this->getApp()->flush();
        Route::clearResolvedInstances();
    }
}
