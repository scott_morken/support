<?php

namespace Tests\Smorken\Support\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\Support\Str;

class StrTest extends TestCase
{
    public function testFriendlyNameWithCamelCase()
    {
        $str = 'thisIsATestOfCamelCase';
        $this->assertEquals('This Is A Test Of Camel Case', Str::friendlyName($str));
    }

    public function testFriendlyNameWithID()
    {
        $str = 'id';
        $this->assertEquals('ID', Str::friendlyName($str));
    }

    public function testFriendlyNameWithSnakeCase()
    {
        $str = 'this_is_a_test_of_snake_case';
        $this->assertEquals('This Is A Test Of Snake Case', Str::friendlyName($str));
    }

    public function testFriendlyNameWithSnakeCaseID()
    {
        $str = 'snake_case_id';
        $this->assertEquals('Snake Case ID', Str::friendlyName($str));
    }

    public function testFriendlyNameWithStudlyCase()
    {
        $str = 'this-is-a-test-of-studly-case';
        $this->assertEquals('This Is A Test Of Studly Case', Str::friendlyName($str));
    }

    public function testFriendlyNameWithStudlyCaseCanSkipDash()
    {
        $str = 'this-is-a-test-of-studly-case';
        $this->assertEquals('This-Is-A-Test-Of-Studly-Case', Str::friendlyName($str, ['_']));
    }
}
