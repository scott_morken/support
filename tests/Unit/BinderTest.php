<?php

namespace Tests\Smorken\Support\Unit;

use Illuminate\Container\Container;
use PHPUnit\Framework\TestCase;
use Smorken\Support\Binder;
use Tests\Smorken\Support\Stubs\ContractBar;
use Tests\Smorken\Support\Stubs\ContractOne;
use Tests\Smorken\Support\Stubs\ImplBar;
use Tests\Smorken\Support\Stubs\ImplOne;
use Tests\Smorken\Support\Stubs\ImplThree;
use Tests\Smorken\Support\Stubs\ImplTwo;

class BinderTest extends TestCase
{
    public function testNoConstructorArgsImplementation()
    {
        [$sut, $app] = $this->getSut();
        $config = [
            'concrete' => [
                ImplThree::class => [],
            ],
            'contract' => [
                ContractOne::class => ImplThree::class,
            ],
        ];
        $sut->bindAll($config);
        $implone = $app[ContractOne::class];
        $this->assertEquals('foo impl three', $implone->foo());
        $this->assertEquals('bar impl three', $implone->bar());
    }

    public function testSimpleImplementation()
    {
        [$sut, $app] = $this->getSut();
        $sut->bindAll($this->getConfig());
        $implone = $app[ContractOne::class];
        $this->assertEquals('foo impl one', $implone->foo());
        $this->assertEquals('bar impl one', $implone->bar());
    }

    public function testWithPreBoundDependency()
    {
        [$sut, $app] = $this->getSut();
        $config = [
            'concrete' => [
                ImplBar::class => [
                    'bar' => 'bar from prebound impl bar in impl two',
                ],
                ImplTwo::class => [
                    'foo' => 'foo impl two prebound',
                    'bar' => [
                        'bound' => ContractBar::class,
                    ],
                ],
            ],
            'contract' => [
                ContractBar::class => ImplBar::class,
                ContractOne::class => ImplTwo::class,
            ],
        ];
        $sut->bindAll($config);
        $impltwo = $app[ContractOne::class];
        $this->assertEquals('foo impl two prebound', $impltwo->foo());
        $this->assertEquals('bar from prebound impl bar in impl two', $impltwo->bar());
    }

    public function testWithReflectiveDependency()
    {
        [$sut, $app] = $this->getSut();
        $override = [
            'contract' => [
                ContractOne::class => ImplTwo::class,
            ],
        ];
        $sut->bindAll($this->getConfig($override));
        $impltwo = $app[ContractOne::class];
        $this->assertEquals('foo impl two', $impltwo->foo());
        $this->assertEquals('bar from impl bar in impl two', $impltwo->bar());
    }

    protected function getConfig($override = [])
    {
        $config = [
            'concrete' => [
                ImplOne::class => [
                    'foo' => 'foo impl one',
                ],
                ImplTwo::class => [
                    'foo' => 'foo impl two',
                    'bar' => [
                        'impl' => ImplBar::class,
                        'params' => [
                            'bar' => 'bar from impl bar in impl two',
                        ],
                    ],
                ],
            ],
            'contract' => [
                ContractOne::class => ImplOne::class,
            ],
        ];

        return array_replace_recursive($config, $override);
    }

    protected function getSut()
    {
        $app = new Container;

        return [new Binder($app), $app];
    }
}
