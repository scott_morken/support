<?php

namespace Tests\Smorken\Support\Stubs;

class ImplOne implements ContractOne
{
    protected $foo;

    public function __construct($foo)
    {
        $this->foo = $foo;
    }

    public function bar(): string
    {
        return 'bar impl one';
    }

    public function foo(): string
    {
        return $this->foo;
    }
}
