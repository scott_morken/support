<?php

namespace Tests\Smorken\Support\Stubs;

class ImplBar implements ContractBar
{
    protected $bar;

    public function __construct($bar)
    {
        $this->bar = $bar;
    }

    public function bar(): string
    {
        return $this->bar;
    }
}
