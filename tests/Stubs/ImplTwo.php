<?php

namespace Tests\Smorken\Support\Stubs;

class ImplTwo implements ContractOne
{
    protected $foo;

    /**
     * @var \Tests\Smorken\Support\Stubs\ContractBar
     */
    protected $bar;

    public function __construct($foo, ContractBar $bar)
    {
        $this->foo = $foo;
        $this->bar = $bar;
    }

    public function bar(): string
    {
        return $this->bar->bar();
    }

    public function foo(): string
    {
        return $this->foo;
    }
}
