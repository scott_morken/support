<?php

namespace Tests\Smorken\Support\Stubs;

interface ContractOne
{
    public function foo(): string;

    public function bar(): string;
}
