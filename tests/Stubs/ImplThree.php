<?php

namespace Tests\Smorken\Support\Stubs;

class ImplThree implements ContractOne
{
    public function bar(): string
    {
        return 'bar impl three';
    }

    public function foo(): string
    {
        return 'foo impl three';
    }
}
