<?php

namespace Tests\Smorken\Support\Stubs;

use Illuminate\Routing\Router;
use Smorken\Support\LoadRoutes;

class LoadRoutesStub extends LoadRoutes
{
    protected function loadRoutes(Router $router): void
    {
        $router->get('/', ['FooController', 'index']);
        $router->post('/', ['FooController', 'doIndex']);
        $router->get('/bar', ['FooController', 'bar']);
    }
}
