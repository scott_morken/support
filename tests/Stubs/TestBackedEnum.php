<?php

declare(strict_types=1);

namespace Tests\Smorken\Support\Stubs;

use Smorken\Support\Constants\Attributes\Concerns\EnumHasLabel;
use Smorken\Support\Constants\Attributes\EnumLabel;

enum TestBackedEnum: string
{
    use EnumHasLabel;

    #[EnumLabel('This is one')]
    case ONE = 'one';

    case TWO = 'two';
}
