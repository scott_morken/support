<?php

namespace Tests\Smorken\Support\Stubs;

interface ContractBar
{
    public function bar(): string;
}
