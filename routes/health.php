<?php

use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;

Route::get('/healthz', function () {
    $time = 0;
    if (defined('LARAVEL_START')) {
        $time = round((microtime(true) - LARAVEL_START) * 1000);
    }

    return Response::json(['status' => true, 'time' => $time], 200, [
        'Content-Type' => 'application/json',
        'Cache-Control' => 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0',
    ]);
});
